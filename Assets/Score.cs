﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Score : MonoBehaviour
{
    public DoorSystem door1; //дверь
    public DoorSystem door2; //дверь
    int score = 0; //стартовый счёт
    int scene_number; //номер сцены текущей

    void Start()
    {
            scene_number = SceneManager.GetActiveScene().buildIndex; //получаем номер сцены
    }

    public void AddScore(int add)
    {
        score = score + add; //функция добавления очков к счёту за разные сделанные манёвры гг
    }

    void Update ()
    {
        //score++;
        this.GetComponent<UnityEngine.UI.Text>().text = System.Convert.ToString(score); //переводим счёт из int в string для отображения на GUI
        if (score >= 500)
        {
            door1 = GameObject.Find("Door").GetComponent<DoorSystem>();
            door1.Setopen(); //если счёт достиг 500 — открываем дверь
            if (scene_number == 2) //на второй сцене две двери, проверяем, есть ли что открывать вообще
            {
                door2 = GameObject.Find("Door (1)").GetComponent<DoorSystem>();
                door2.Setopen();
            }
        }
    }
}


