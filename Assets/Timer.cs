﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float timer; //переменная таймера
    public GameObject Kukuruza; //игровой объект бонуса

    void Start()
    {
        
    }

    void Update()
    {
        if (timer > 0) 
        {
            timer -= Time.deltaTime; //если таймер больше нуля, то уменьшаем его
        }
        if (timer <= 0) //пока таймер работает, ставим бонус
        {
            if (Kukuruza != null) //если нет бонуса, то заствляем его появиться на карте
                Kukuruza.SetActive(true); 

        }
    }
}
