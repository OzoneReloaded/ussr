﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    public float StartTime;
    public float EndTime; //переменные начала работы счётчика и конца его работы
    public Score score; //объявляем класс счёта


    void Start()
    {
        
    }

    void FixedUpdate() //базовая функция юнити, которая выполняет своё тело при каждом обновлении кадра
    {
        StartTime += 0.1f * Time.deltaTime; //здесь мы указываем, что время таймера не должно зависеть от структуры того, как юнити считает время
        //юнити считает время в зависимости от того как быстро мелькают кадры
        //нам это не надо
        
        if (StartTime >= EndTime)
        {
            Destroy(gameObject); //уничтожаем объект после конца работы таймера
        }
    }

    void OnTriggerEnter2D(Collider2D co)
    {
        if (co.name == "lenin") //проверяем, если после коллизии имя того, что коллизировало с бонусом = гг, то
        {
            Destroy(this.gameObject); //убираем бонус
            score = GameObject.Find("scoreNum").GetComponent<Score>(); //получаем информацию о компоненте счёта
            score.AddScore(400); //добавляем к нему 400 очков
        }
    }
}
