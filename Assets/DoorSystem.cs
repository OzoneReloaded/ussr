﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSystem : MonoBehaviour
{
    public bool isOpen = false; //открыта ли дверь?
    public Sprite doorclose; //спрайт закрытой двери
    public Sprite dooropen; //открытой

    public void Setopen() //функция, которая "открывает" дверь
    {
        this.GetComponent<SpriteRenderer>().sprite = dooropen; //при "открытии" меняем спрайт на открытую дверь
        this.isOpen = true; //открыта ли дверь!
        this.GetComponent<Collider2D>().enabled = false;//убираем коллайдер двери, чтобы через неё можно было идти, вуаля!
    }

    void Update()
    {
        
    }
}
