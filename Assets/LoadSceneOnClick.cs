﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

	public void LoadByIndex(int sceneIndex) //загрузка уровня по нажатию
	{
		SceneManager.LoadScene (sceneIndex);
	}
}