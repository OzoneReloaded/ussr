﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeLevel : MonoBehaviour
{
    public int scene_number; //номер текущей сцены
    void Start()
    {
        scene_number = SceneManager.GetActiveScene().buildIndex; //получаем его по индексу в списке на билд
    }

    void OnTriggerEnter2D(Collider2D hero)
    {
        if (hero.name == "lenin") //если гг сталкивается с невидимым объектом, то уровень меняется
        {
            if (scene_number != 4)
            {
                SceneManager.LoadScene(scene_number + 1); //загружаем уровень следующий
            }
            else
            {
                SceneManager.LoadScene(0); //если уровень последний, идём в меню
            }
        }
    }
}
