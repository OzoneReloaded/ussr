﻿using UnityEngine;
using System.Collections;

public class QuitOnClick : MonoBehaviour { //наследование класса скрипта от базового класса монобихевиора

	public void Quit() //функция выхода
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit ();
		#endif
	}

}