﻿using UnityEngine;
using System.Collections;

public class MovePacman : MonoBehaviour //наследование класса скрипта от базового класса монобихевиора
{
	public float speed = 10f;
	Vector2 dest = Vector2.zero; //переменные отвечающие за скорость и перемещение в пространстве

	void Start()
	{
		dest = transform.position; //указываем при инициализации, что dest (конечная позиция) является изменением начальной
	}

	void FixedUpdate() { //базовая функция юнити, которая выполняет своё тело при каждом обновлении кадра
		 //перемещаем цель к точке, куда требуется переместить
		Vector2 p = Vector2.MoveTowards(transform.position, dest, speed);
		GetComponent<Rigidbody2D>().MovePosition(p);

		//если цель не двигаемся, то поочерёдно проверяем ввод с клавиатуры, в зависимости от котрого изменяем конечное положение в зависимости от нажатой кнопки
        //вверх, вправо, влево и вниз
        //так для влево и вниз используем отрицательные числа
		if ((Vector2)transform.position == dest) {
			if (Input.GetKey(KeyCode.UpArrow) && valid(Vector2.up))
				dest = (Vector2)transform.position + Vector2.up;
			if (Input.GetKey(KeyCode.RightArrow) && valid(Vector2.right))
				dest = (Vector2)transform.position + Vector2.right;
			if (Input.GetKey(KeyCode.DownArrow) && valid(-Vector2.up))
				dest = (Vector2)transform.position - Vector2.up;
			if (Input.GetKey(KeyCode.LeftArrow) && valid(-Vector2.right))
				dest = (Vector2)transform.position - Vector2.right;
		}
	} 

	void OnCollisionEnter2D(Collision2D collision) //базовая функция юнити, которая срабатывает при условии, что коллайдеры объектов столкнулись
	{
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;  //если столкнулись, то скорость нашего гг нулевая
	}

	 bool valid(Vector2 dir)
	{
		//функция выполняет проверку, есть ли преграда после гг в виде стены или же пространство свободно
        //это нужно для того, чтобы гг не застрявал в стенах при каждокадровой проверке на столкновения
		Vector2 pos = transform.position;
		RaycastHit2D hit = Physics2D.Linecast(pos + dir, pos);
		return (hit.collider == GetComponent<Collider2D>());
	} 

}