using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GhostMove : MonoBehaviour {
	public Transform[] waypoints; //массив компонентов вейпоинтов для врагов
	int cur = 0; //текущий вейпоинт
	public float speed = 0.3f; //скорость движения врага
    public GameObject Kukuruza; //всё тот же бонус
    public Score score; //всё тот же класс счёта

    void FixedUpdate () {
		//покадровая проверка, не достигли ли мы вейпоинта? если нет, идём дальше по кратчайшему пути
		 if (transform.position != waypoints[cur].position) {
			Vector2 p = Vector2.MoveTowards(transform.position,
				waypoints[cur].position,
				speed);
			GetComponent<Rigidbody2D>().MovePosition(p);
		}
		//достигли? идём к следующему
		else cur = (cur + 1) % waypoints.Length;
	}  

	void OnTriggerEnter2D(Collider2D co) {
        if (co.name == "lenin")
        {
             
            Destroy(this.gameObject); //уничтожаем врага, если гг настиг его
            score = GameObject.Find("scoreNum").GetComponent<Score>();
            score.AddScore(200); //добавляем счёт
            
        }
        if (Kukuruza != null)
        {
            Kukuruza.SetActive(true); //если нет бонуса, то спавним его
        }
    }
}